const { Builder, By, Capabilities } = require('selenium-webdriver')
const { getImageURLs, downloadImages } = require('./libs')

const main = async () => {
  const values = process.argv.slice(3)
  const limit = Number(process.argv[2])

  if (isNaN(limit)) {
    values.unshift(process.argv[2])
  }

  const chromeCapabilities = Capabilities.chrome();

  //Setting chrome options
  chromeCapabilities.set("goog:chromeOptions", {
    args: [
      "headless",
    ]
  });

  const driver = await new Builder()
    .forBrowser("chrome")
    .withCapabilities(chromeCapabilities)
    .build();
  for await (const value of values) {
    console.log("Searching for: " + value)
    try {
      await driver.get("https://images.google.com/")
      await driver.findElement(By.name("q")).sendKeys(value)
      await driver.findElement(By.css(".FAuhyb svg")).click()

      const result = await driver.executeAsyncScript(getImageURLs)
      await downloadImages(result, value, limit)
    } catch (error) {
      console.log(error)
    }
    console.log("\n\n")
  }
  await driver.quit()
}

main()