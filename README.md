## Cài đặt
```
yarn
```

## Sử dụng

```
yarn get <limit> <...image search>
```
Ví dụ: Lấy 100 ảnh con chó, con mèo, con rùa
```
yarn get 100 "con chó" "con mèo" "con rùa"
```
Ví dụ: Lấy tất cả ảnh con chó, con mèo, con rùa
```
yarn get "con chó" "con mèo" "con rùa"
```

